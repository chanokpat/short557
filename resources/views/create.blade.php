@extends('layouts.app')
@section('content')

    <h1>Place URL here</h1>
    <form method="post" action="{{ url('/') }}">
        @csrf

        <div class="form-group">
            <label>Long URL</label>
            <input type="text" name="long_url" class="form-control">
        </div>

        <button class="btn btn-primary" type="submit">CREATE SHORT URL</button>
    </form>
@endsection

