@extends('layouts.app')
@section('content')

    <div class="title-box" style="width: 90%; float: left;">
        <h3 class="display-4" role="alert">LIST URL</h3>
    </div>

    <div class="title-box mt-2" style="width: 10%;float: left;">
        <a href="{{url('/new')}}"><button type="button" id="button-addon2" class="btn btn-warning mb-4">CREATE URL</button></a>
    </div>


    @if(count($todos)>0)
        @foreach($todos as $todo)
            <div>
                <a href="{{url($todo->long_url)}}">
                    <h5 class="text-danger">{{$todo->long_url}}</h5>
                </a>


                <div class="input-group mb-1">
                    <button onclick="copy(this)" value="{{$todo->id}}" type="button" id="button-addon2" class="btn btn-outline-danger">COPY</button>

                        <input id="shortenurl{{$todo->id}}" class="form-control text-info outline-danger" type="text"  value="http://www.short.local/t/{{$todo->short_url}}" readonly>

                    <div class="input-group-append">
                        <span class="input-group-text">{{$todo->created_at}}</span>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text">view : {{$todo->view}}</span>
                    </div>
                </div>

            </div>
            <hr>
        @endforeach
    @endif

    <script>
        function copy(clickedBtn){
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shortenurl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied ' + copyText.value);
        }
    </script>



@endsection
